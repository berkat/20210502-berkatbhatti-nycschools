//
//  Constants.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//

import Foundation
// Constant file will hold string and other constant values for this project to prevent crashing

struct K {
    struct TableViewIdenitifiers {
        static let schoolCell = "SchoolCell"
    }
    struct Segues {
        static let schoolsToSat = "schoolToSATResults"
    }
}
