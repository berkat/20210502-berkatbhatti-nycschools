//
//  SatResultsVC.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//

import UIKit

class SatResultsVC: UIViewController {
    @IBOutlet weak var numberOfaTakers: UILabel!
    @IBOutlet weak var readingAverage: UILabel!
    @IBOutlet weak var mathAverage: UILabel!
    @IBOutlet weak var writingAverage: UILabel!
    @IBOutlet weak var schoolName: UILabel!
    
    
    var satResult: SAT?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
  
    
    func updateUI() {
        // Another Check that the sat reault is not nil and was successfully passed, if not return out of the function
        guard satResult != nil else {return}
        self.schoolName.text = satResult?.school_name.capitalized
        self.numberOfaTakers.text = "Number of test takers: \(satResult?.num_of_sat_test_takers ?? "0")"
        self.readingAverage.text = "Critical reading average Score: \(satResult?.sat_critical_reading_avg_score ?? "0")"
        self.mathAverage.text = "Math average score: \(satResult?.sat_math_avg_score ?? "0")"
        self.writingAverage.text = "Writing average score: \(satResult?.sat_writing_avg_score ?? "0")"
    }
}
