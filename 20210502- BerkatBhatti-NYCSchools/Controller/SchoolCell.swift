//
//  SchoolCell.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//

import UIKit

class SchoolCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var schoolContainer: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        schoolContainer.layer.cornerRadius = schoolContainer.frame.height / 8
    }
    // Function will update cell with school passed
    func updateSchoolCell(_ school: School) {
        self.nameLabel.text = school.school_name
        self.locationLabel.text = school.location
        self.overviewLabel.text = school.overview_paragraph
    }
}
