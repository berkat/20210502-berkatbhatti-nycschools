//
//  SchoolsVC.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//

import UIKit

class SchoolsVC: UIViewController {

    @IBOutlet weak var schoolsTableView: UITableView!
    @IBOutlet weak var schoolCountButton: UIBarButtonItem!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
   // Setting golbal var that is set to be assigned
    var schools = [School]()
    var satResult: SAT?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // We only want the spinner to appear once with View did load(called once) 
        spinner.isHidden = false
        spinner.startAnimating()
        SchoolService.delegate = self
        SchoolService.getSchoolsListForNYC()
        // Register Nib File for School Cell
        schoolsTableView.register(UINib(nibName: K.TableViewIdenitifiers.schoolCell, bundle: nil), forCellReuseIdentifier: K.TableViewIdenitifiers.schoolCell)
    }

}


// Inheriting from the school delegate
extension SchoolsVC: SchoolDelegate {
    // Func to get returned sat results for the selected school
    func didFinishGettingSatData(sat: SAT?) {
        DispatchQueue.main.async {
            guard sat != nil else {
                let alert = UIAlertController(title: "No Results Found", message: "We are updating each school SAT results every week. Unfortunately scores for the school selected are being updated. For the moment, Please Select Another School!. We apologize for the inconvenience.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Got It", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.satResult = sat
            // Performing segue only after data downloaded fro mthe background is completed
            self.performSegue(withIdentifier: K.Segues.schoolsToSat, sender: self)
        }
    }
    func didFinishGettingSchoolData(schools: [School]) {
        // Updating returned results on the main thread
        DispatchQueue.main.async {
            self.schools = schools
            self.schoolCountButton.title = "\(schools.count) found"
            self.schoolsTableView.reloadData()
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
            
        }
    }
}

// extension to conform to tableView protocol
extension SchoolsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schools.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let school = schools[indexPath.row]
        guard let schoolCell = schoolsTableView.dequeueReusableCell(withIdentifier: K.TableViewIdenitifiers.schoolCell, for: indexPath) as? SchoolCell else {return UITableViewCell()}
        schoolCell.updateSchoolCell(school)
        return schoolCell
    }
    // Will only look for SAT results for selected school dbn value
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = schools[indexPath.row]
        SchoolService.getSATresultsFor(for: school.dbn ?? "")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == K.Segues.schoolsToSat {
            guard let resultsVC = segue.destination as? SatResultsVC else {return}
            resultsVC.satResult = self.satResult
        }
    }
}
