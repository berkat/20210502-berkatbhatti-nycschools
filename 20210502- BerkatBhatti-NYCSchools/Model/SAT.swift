//
//  SAT.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//



import Foundation

// Strut will capture the School Name and the SAT data
// The dbn value will be used as an identifier to match the SAT data with the School data
// Conventional camel casing was not used to properly assoiated the decoadable protocol with the Json format
struct SAT: Decodable {
    var school_name: String
    var num_of_sat_test_takers: String
    var sat_critical_reading_avg_score: String
    var sat_math_avg_score: String
    var sat_writing_avg_score :String
    // Will use an optional for the dbn since we are not certain all schools have one
    var dbn: String?
    
}
