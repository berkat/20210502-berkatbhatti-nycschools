//
//  School.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//

import Foundation




// Creating school structure
// Consiting of the name and the address of the school in NYC
// Will Inherit fro the  Decodable Protocal for JSon decoding
// Conventional camel casing was not used to properly assoiated the decoadable protocol with the Json format
struct School: Decodable {
    // Assuming that all schools will have name and location provided but may not have an overview
    let school_name: String
    var overview_paragraph: String?
    let location: String
    // Will use DBN values to get SAT results instead of school names to prevent error
    // Because both APIs have the DBN parameter with this will make out call more accurate
    let dbn: String?
}
