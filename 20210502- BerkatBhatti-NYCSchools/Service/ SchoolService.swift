//
//   SchoolService.swift
//  20210502- BerkatBhatti-NYCSchools
//
//  Created by Berkat Bhatti on 5/2/21.
//

import Foundation




/* Service file will fetch the school data from Json url provided
 
 When approaching this task I discovered that the results from both Api calls were not of equal value when checking the count. I left a print statemnt whhen testing the app to show that the school list was 38 less than the SAT detail api. Initial approach was to check if the school_name in the first call matched that of the SAT results call but soon realize that there were some schools named differently in the Json results(Capitalized also), I decided the dbn could be used as an identifier for the schools and my have been a better approach. This way the worry of a capitalization/lowercased issue could be resolved. The Dbn for the SAT results was compared to that of the school passed and if matched the SAT resulted were passed. IF not I used to and alert to present to the user the school data is being updated. This would handle all the cases where the dbns did not match or data was missing.  Again this may not be the best approach to allow our users to see schools with no data but it assisted with preventing a crash in the app.
 
 
    With more time it would have been possible to create a dictionary of [String: String], that ould consist of the dbns and the school name. This way we could ony display schools where we had the associated dbns.  Suggestion would be to update the Api to make sure the dbns match that of each school. Also to make sure the datasase is balanced with the proper amount of schools.
 
 */

fileprivate let schoolsUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
fileprivate let schoolsDetailsUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"



// We set a protocol to handle data passing for th schools
protocol SchoolDelegate {
    func didFinishGettingSchoolData(schools: [School])
    func didFinishGettingSatData(sat: SAT?)
    // We could also set a function to handle errors and present an alert to the user, but for time purposes we will leact the errors as print statemtents.
}

struct SchoolService {
    
    static var delegate: SchoolDelegate?
    
    //Func to get fetch schools list for NYC
    static func getSchoolsListForNYC() {
        guard let url = URL(string: schoolsUrl) else {return}
        // Creating a urlSession instance to fetch data
        let session = URLSession(configuration: URLSessionConfiguration.default)
        session.dataTask(with: url) { (data, response, error) in
            if let err = error {
                // There was an error
                print("There was an error fetch the data for the url \(err)")
            } else {
                //There was no error
                guard let returnedData = data else {return}
                guard let allShools = decode(data: returnedData) else {return}
                print("The school count is \(allShools.count)")
                // Delegate function will pass the returned schools data to VC where delegate is assigned
                self.delegate?.didFinishGettingSchoolData(schools: allShools)
            }
        }.resume()
    }
    // When taking this route we see that there is a mismatch between the dbn. The List of schools also are of of unequal values
    static func getSATresultsFor(for schoolDbn: String) {
        guard let satUrl = URL(string: schoolsDetailsUrl) else {return}
        let session = URLSession(configuration: .default)
        session.dataTask(with: satUrl) { (data, response, error) in
            if let err = error {
                print("There was an error \(err)")
            } else {
                //There was no error
                guard let returnedResults = data else {return}
                guard let satResults = decodeSAT(data: returnedResults) else {return}
                print("Here is the satCount \(satResults.count)")
                // Will use an array method to check if the dbn is == to that of the school we are interested in/selecting
                // with more time a dictionary may be a beter approach here to make sure the dbn exist in both data calls.
                let schoolSATResult = satResults.first { sat -> Bool in
                    sat.dbn == schoolDbn
                }
                self.delegate?.didFinishGettingSatData(sat: schoolSATResult)
            }
        }.resume()
        
    }
    /// Private function to decode SAT results
    private static func decodeSAT(data: Data) -> [SAT]? {
        let decoder = JSONDecoder()
        do {
            let decodedSAT = try decoder.decode([SAT].self, from: data)
            return decodedSAT
        } catch {
            // There was a problem decoding
            print("There was an error decding SAT data \(error.localizedDescription)")
            return nil
        }
    }
    
    
    
    // Function that will decode the returned Json Data
    private static func decode(data: Data) -> [School]? {
        let decoder = JSONDecoder()
        do {
            let decodedSchool = try decoder.decode([School].self, from: data)
            return decodedSchool
        } catch {
            // The was an error
            print("There was an error decoding the data \(error.localizedDescription)")
            return nil
        }
    }
    
    
    
    
    
    
    
}
