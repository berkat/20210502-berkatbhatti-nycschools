# 20210502- BerkatBhatti-NYCSchools

This mobile application display a list of NYC High Schools with additional details when the school is selected. 


When approaching this task I discovered that the results from both Api calls were not of equal value when checking the count. I left a print statemnt whhen testing the app to show that the school list was 38 less than the SAT detail api. Initial approach was to check if the school_name in the first call matched that of the SAT results call but soon realize that there were some schools named differently in the Json results(Capitalized also), I decided the dbn could be used as an identifier for the schools and my have been a better approach. This way the worry of a capitalization/lowercased issue could be resolved. The Dbn for the SAT results was compared to that of the school passed and if matched the SAT resulted were passed. IF not I used to and alert to present to the user the school data is being updated. This would handle all the cases where the dbns did not match or data was missing.  Again this may not be the best approach to allow our users to see schools with no data but it assisted with preventing a crash in the app.


   With more time it would have been possible to create a dictionary of [String: String], that could consist of the dbns and the school name. This way we could ony display schools where we had the associated dbns.  Suggestion would be to update the Api to make sure the dbns match that of each school. Also to make sure the datasase is balanced with the proper amount of schools. 
   
   Also Looking at the MVC approached Implimented the Objects for SAT could inherit from the School object. This would require us to use a class instead of a struct since both Json objects share the school_name and dbn parameter. This would be a better approach but due to lack of time I could not revert to taking this approach. Could also make the Decodable objects a seprate object which would allow me to keep the School and SAT objects in camel Case format and assign the results for the decoded class to these objects. 
